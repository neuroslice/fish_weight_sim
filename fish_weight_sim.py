import csv
import math

def simulation(weight):
	
	# Set up initial values
	weight_cur = weight
	alpha = 0.038
	beta = 0.6667
	tau = 0.08
	
	# Open the csv and read it in
	with open('temperature_series.csv', newline='') as csvfile:
		reader = csv.DictReader(csvfile, delimiter=',')
		
		for row in reader:
			
			# Grab the current temperature from the CSV
			temp_cur = float(row['Temp'])
			
			#Perform the iterative calculation
			weight_cur = (alpha * weight_cur**beta * math.e**(temp_cur * tau)) + weight_cur
			
			# Commented out current values for debugging
			#print ("Current Temp: " + str(temp_cur))
			#print ("Current Weight: " + str(weight_cur))
	
	# Return final weight
	return weight_cur

if __name__ == "__main__":

	# Hard coded initial fish weight
	testWeight = 20

	print ("Initial weight: " + str(testWeight) + " grams.")
	print ("Final weight: " + str(simulation(testWeight)) + " grams.")
